import React, { Component } from 'react'

import Modal from 'react-bootstrap/Modal'

class ModalView extends Component {
    handleHide = () => {
        //  this.props.handleModalShow(null)
        console.log('closing')
    }
    componentDidMount() {
        console.log(this.props)
    }
    componentDidUpdate() {
        console.log(this.props)
    }
    render() {
        let title,
            copy,
            specialCopy,
            endCopy,
            secondSet,
            thirdSet,
            links = null
        if (this.props.show) {
            console.log(this.props.modalData)
            let { modalTitle, modalCopy, modalList } = this.props.modalData
            title = <h2>{modalTitle}</h2>
            copy = modalCopy.map((p, index) => <p key={index}>{p}</p>)
            links = modalList.map((link, index) => (
                <li key={index}>
                    <a
                        href={link.linkTarget}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        {link.linkTitle}
                    </a>
                </li>
            ))
            if (this.props.modalData.specialCopy) {
                specialCopy = (
                    <p>
                        <strong>
                            {this.props.modalData.specialCopy.title}
                        </strong>{' '}
                        <br />
                        {this.props.modalData.specialCopy.copy}
                    </p>
                )
            } else {
                specialCopy = null
            }
            if (this.props.modalData.endCopy) {
                endCopy = (
                    <p>
                        {this.props.modalData.endCopy.copy}
                        <a
                            href={this.props.modalData.endCopy.linkTarget}
                            target="_blank"
                            rel="noopener noreferrer"
                            className="small-link"
                        >
                            {this.props.modalData.endCopy.linkTitle}
                        </a>
                    </p>
                )
            } else {
                endCopy = null
            }
            if (this.props.modalData.secondSet) {
                let ssData = this.props.modalData.secondSet
                secondSet = (
                    <div className="additional-copy-modal">
                        <h3>{ssData.title}</h3>
                        <p>{ssData.copy}</p>
                        <a
                            href={ssData.link.target}
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            {ssData.link.copy}
                        </a>
                    </div>
                )
            } else {
                secondSet = null
            }
            if (this.props.modalData.thirdSet) {
                let tsData = this.props.modalData.thirdSet
                thirdSet = (
                    <div className="additional-copy-modal">
                        <h3>{tsData.title}</h3>
                        <p>{tsData.copy}</p>
                        <a
                            href={tsData.link.target}
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            {tsData.link.copy}
                        </a>
                    </div>
                )
            }
        }
        return (
            <div>
                <Modal
                    show={this.props.show}
                    onHide={this.props.hideMod}
                    dialogClassName="modal-90w"
                    size="lg"
                    centered
                    className="wtsu-modal"
                >
                    <Modal.Header closeButton />
                    <Modal.Body>
                        {title}
                        {copy}
                        {specialCopy}
                        <ul>{links}</ul>
                        {secondSet}
                        {thirdSet}
                        {endCopy}
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}

export default ModalView
