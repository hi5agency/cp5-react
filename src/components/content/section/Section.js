import React, { Component } from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import './Section.css'

class Section extends Component {
    state = {}
    handleModalShow(data){
        if(
            this.state.modalData === null
            &&
            !this.state.showModal
        ){
            this.setState({
                modalData:data,
                showModal:true
            })
        }else{
            this.setState({
                showModal:false,
                modalData:null
            })
        }
    }
    render() {
        let copyColOrder, imageColOrder, textAlign, borderPosition
        if (this.props.rightSideImage) {
            copyColOrder = 1
            imageColOrder = 10
            textAlign = 'text-right'
            borderPosition='justify-content-end header-line hl-right'
        } else {
            copyColOrder = 10
            imageColOrder = 1
            textAlign = ''
            borderPosition='justify-content-start header-line hl-left'
        }

        const list = this.props.episodeLinks.map((link, index) => (
            <li
                key={index}
                onClick={()=> this.props.catchData(link)}
            >
                {link.listTitle}
            </li>
        ))

        return (
            <Row className="content__section align-items-center">
                <Col
                    md={{ span: 6, order: imageColOrder }}
                    className="section__image"
                >
                    <img
                        src={this.props.sectionImage}
                        alt={this.props.sectionImageTitle}
                    />
                </Col>
                <Col
                    md={{ span: 6, order: copyColOrder }}
                    className={`section__copy ${textAlign}`}
                >
                    <h3>
                        <span>{this.props.episode}</span> <br />
                        {this.props.title}

                    </h3>
                    <Row className={borderPosition}>
                        <div className="title-break col-1">
                            <span>&nbsp;</span>
                        </div>
                    </Row>
                    <p>{this.props.copy}</p>
                    <ul>{list}</ul>
                </Col>
            </Row>
        )
    }
}

export default Section
