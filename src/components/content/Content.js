import React, { Component } from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import './Content.css'
import Section from './section/Section'


import EpisodeData from '../../assets/ContentData'

// section image names bellow
// courtSystem
// incarceration
// policeInteraction
// postIncarceration

class Content extends Component {
    state = {}
    catchData = (listData) => {
        console.log(listData);
        this.props.getModalData(listData)
    }
    render() {
        const sections = EpisodeData.map((episode, index) => (
            <Section
                key={index}
                sectionImage={episode.image}
                sectionImageTitle={episode.title}
                rightSideImage={episode.rightSideImage}
                episode={episode.episode}
                title={episode.title}
                copy={episode.copy}
                episodeLinks={episode.episodeLinks}
                catchData={this.catchData}
            />
        ))
        return (
            <div className="content">
                <Row className="content__title">
                    <Col sm={12}>
                        <h2>ways to get involved</h2>
                    </Col>
                </Row>
                <div>{sections}</div>
            </div>
        )
    }
}

export default Content
