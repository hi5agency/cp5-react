import React, { Component } from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import HeaderWTSU from '../../assets/header-wtsu.png'
import HeaderCOC from '../../assets/header-coc.png'

import './Header.css'

class Header extends Component {
    state = {}
    render() {
        return (
            <div className="header">
                <Row className="header__logos">
                    <Col sm={6} md={2} className="logo-wrap">
                        <a
                            href="https://www.netflix.com/title/80200549"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            <img src={HeaderWTSU} alt="When They See Us" />
                        </a>
                        <img src={HeaderCOC} alt="Change of Color" />
                    </Col>
                </Row>

                <Row className="header__title justify-content-md-center">
                    <Col sm={8} xl={6} className="title-wrap">
                        <h1>
                            Taking Action <br /> <span>In Today's America</span>
                        </h1>
                    </Col>
                </Row>
                <Row className="header__copy justify-content-md-center">
                    <Col sm={8} xl={7}>
                        <p>
                            When They See Us tells the true story of the Central
                            Park Five and their unjust experiences with the
                            criminal justice system in America, a reality that
                            millions of Americans still face today. The
                            following guide is a step-by-step introduction to
                            organizations working to reform our current criminal
                            justice system and how you can join the
                            conversation, learn more and take action.{' '}
                        </p>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default Header
