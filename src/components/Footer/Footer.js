import React, { Component } from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import './Footer.css'

import ChangeOfColor from '../../assets/change-of-color.png'
import Participant from '../../assets/participant.png'
import WinningJustice from '../../assets/winning-justice.png'

class Footer extends Component {
    state = {}
    render() {
        return (
            <Row className="footer">
                <Col sm={12} className="text-center footer-copy">
                    <h2>TAKE ACTION. LEARN MORE.</h2>
                </Col>
                <Col
                    md={{ span: 2, offset: 3 }}
                    className="footer__image text-center"
                >
                    <a
                        href="https://colorofchange.org/"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <img src={ChangeOfColor} alt="Change Of Color" />
                    </a>
                </Col>
                <Col md={2} className="footer__image text-center">
                    <a
                        href="https://www.participantmedia.com/"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <img src={Participant} alt="participant media" />
                    </a>
                </Col>
                <Col md={2} className="footer__image text-center">
                    <a
                        href="https://www.winningjustice.org/"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <img src={WinningJustice} alt="Winning Justice" />
                    </a>
                </Col>
                <Col sm={12} className="netflix-link text-center">
                    <a
                        href="https://www.netflix.com/title/80200549"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        {' '}
                        Stream When They See Us, based on the true story of the
                        Central Park Five now on Netflix{' '}
                    </a>
                </Col>
            </Row>
        )
    }
}
export default Footer
