import React, { Component } from 'react';
import Container from 'react-bootstrap/Container'

import Header from './components/header/Header'
import Content from './components/content/Content'
import Footer from './components/Footer/Footer'
import ModalView from './components/Modal/ModalView'

import './App.css'


class App extends Component {
    state = {
        showModal:false,
        modalData:null
    }
    getModalData = (data) =>{
        this.setState({modalData:data, showModal:true})
    }
    handleClick = () =>{
        this.setState(prevState =>({
            showModal:!prevState.showModal
        }))
    }


    render() {
        return (
        <Container fluid="true">
            <Header />
            <Content getModalData={this.getModalData}/>
            <Footer />
            <ModalView show={this.state.showModal} modalData={this.state.modalData} hideMod={this.handleClick}/>
        </Container>
         );
    }
}

export default App;
