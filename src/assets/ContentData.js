import courtSystem from './wtsu-court-system.jpg'
import incarceration from './wtsu-incarceration-alt.jpg'
import policeInteraction from './wtsu-police-interaction.jpg'
import postIncarceration from './wtsu-post-incarceration-alt.jpg'
const EpisodeData = [
    {
        rightSideImage: false,
        image: policeInteraction,
        episode: 'Part One',
        title: 'Police Interaction',
        copy:
            'Overpolicing, an abundant police presence in targeted neighborhoods, and the abuse of children during police interrogations, are principal issues in our current criminal justice system. These structural approaches to criminal justice must be confronted and reformed. Here are some organizations that are working on this:',
        episodeLinks: [
            {
                listTitle: 'Know Your Rights',
                modalTitle: 'Know Your Rights',
                modalCopy: [
                    'A free campaign for youth founded by Colin Kaepernick to raise awareness on higher education, self-empowerment, and instruction to properly interact with law enforcement in various scenarios.',
                ],
                modalList: [
                    {
                        linkTitle: 'Get Involved',
                        linkTarget: 'http://bit.ly/2HPRaki',
                    },
                ],
            },
            {
                listTitle: 'ACLU',
                modalTitle: 'ACLU',
                modalCopy: [
                    'The ACLU defends and preserves individual rights through working in courts, communities and legislatures, to ensure all constitutional liberties are guaranteed.',

                    'One of their initiatives is to work on the “front end” of the criminal justice system—from policing to sentencing— seeking to end excessively harsh criminal justice policies that result in mass incarceration, over-criminalization, and racial injustice, and stand in the way of a fair and equal society.',
                ],
                specialCopy: {
                    title: 'Know Your Rights: Stopped by the Police ',
                    copy:
                        'Being stopped by police is a stressful experience that can go bad quickly. Find out what the law requires and also offer strategies for handling the following police encounters:',
                },
                modalList: [
                    {
                        linkTitle: `I’ve been stopped by the police in public `,
                        linkTarget: 'http://bit.ly/2YKWrRe',
                    },
                    {
                        linkTitle: `I’ve been pulled over by the police`,
                        linkTarget: 'http://bit.ly/2VH0hc9',
                    },
                    {
                        linkTitle: `The police are at my door`,
                        linkTarget: 'http://bit.ly/2VZQtPl',
                    },
                    {
                        linkTitle: `I’ve been arrested by the police`,
                        linkTarget: 'http://bit.ly/2Ern4D2',
                    },
                    {
                        linkTitle: `The police violated my rights`,
                        linkTarget: 'http://bit.ly/30xPQuW',
                    },
                ],
            },
            {
                listTitle: 'Equal Justice Initiative',
                modalTitle: 'Equal Justice Initiative',
                modalCopy: [
                    `Support the work of the Equal Justice Initiative, one of the nation's most effective legal and advocacy organizations.`,
                ],
                modalList: [
                    {
                        linkTitle: 'Get Involved',
                        linkTarget: 'http://bit.ly/2Qh6J8D',
                    },
                ],
            },
            {
                listTitle: 'Good Call (NYC)',
                modalTitle: 'Good Call (NYC)',
                modalCopy: [
                    'Phones are taken away when someone is arrested and can leave you without access to your emergency contacts. Good Call preemptively stores your emergency contact information and connects you with a free lawyer upon arrest. Currently New York City based.',
                ],
                modalList: [
                    {
                        linkTitle: 'Learn More',
                        linkTarget: 'http://bit.ly/2VZbqoJ',
                    },
                    {
                        linkTitle: 'Save a Contact and Protect Your Rights',
                        linkTarget: 'http://bit.ly/2EpNkxh',
                    },
                ],
            },
            {
                listTitle: 'Color of Change',
                modalTitle: 'Color of Change',
                modalCopy: [
                    'Color Of Change is the nation’s largest online racial justice organization. The organization helps people respond effectively to injustice in the world around us. As a national online force driven by more than 1.4 million members, Color of Change moves decision-makers in corporations and government to create a more human and less hostile world for Black people in America.',
                ],
                modalList: [
                    {
                        linkTitle: 'Take Part in a Campaign ',
                        linkTarget: 'http://bit.ly/2HRceqD',
                    },
                ],
                endCopy: {
                    copy:
                        'Did you know Prosecutors are elected to their positions? ',
                    linkTitle:
                        'Find out who your local prosecutors are and how they measure up on criminal justice reform.',
                    linkTarget: 'http://bit.ly/2we7YfD',
                },
            },
            {
                listTitle: 'Communities United for Police Reform (NYC)',
                modalTitle: 'Communities United for Police Reform (NYC)',
                modalCopy: [
                    `Communities United for Police Reform (CPR) is an unprecedented campaign to end discriminatory policing practices in New York, bringing together a movement of community members, lawyers, researchers and activists to work for change.`,
                ],
                modalList: [
                    {
                        linkTitle: 'Learn More',
                        linkTarget: 'http://bit.ly/2VYWNSu',
                    },
                ],
            },
        ],
    },
    {
        rightSideImage: true,
        image: courtSystem,
        episode: 'Part Two',
        title: 'COURT SYSTEM',
        copy:
            'The challenges and systemic biases within America’s criminal justice system are vast and complicated, from the abuse of prosecutorial power to the extreme sentencing of children. Here are some of the organizations that are successfully taking on these specific issues:',
        episodeLinks: [
            {
                listTitle: 'Juvenile Law Center',
                modalTitle: 'Juvenile Law Center',
                modalCopy: [
                    'Juvenile Law Center advocates for rights, dignity, equity and opportunity for youth in the child welfare and justice systems.  ',
                ],

                modalList: [
                    {
                        linkTitle: 'Learn More',
                        linkTarget: 'http://bit.ly/2XabpQp',
                    },
                    {
                        linkTitle:
                            'Read More About Interrogations and the Criminal Justice System',
                        linkTarget: 'http://bit.ly/2Wthn1z',
                    },
                ],
            },
            {
                listTitle: 'Community Justice Exchange ',
                modalTitle: 'Community Justice Exchange ',
                modalCopy: [
                    'Criminal Justice Exchange works to develop, share, and experiment with innovative approaches to ending mass incarceration.',
                ],
                modalList: [
                    {
                        linkTitle: 'Learn More',
                        linkTarget: 'http://bit.ly/2VYAhxP',
                    },
                ],
                secondSet: {
                    title: 'National Bail Fund Network',
                    copy:
                        'Community Justice Exchange also provides a National Bail Fund Network with a full directory of community bail and bond funds across America to assist with bailing out loved ones. ',
                    link: {
                        copy: 'Read More about the National Bail Fund Network ',
                        target: 'http://bit.ly/2HOg5ou',
                    },
                },
            },
            {
                listTitle: 'Court Watch NYC',
                modalTitle: 'Court Watch NYC',
                modalCopy: [
                    'Court Watch NYC builds the power of everyday New Yorkers to demand transparency and accountability from the criminal legal system and an end to mass incarceration. By watching court proceedings, reporting what they see, and organizing around the systemic injustices that they witness, Court Watch NYC aims to shift courtroom policies, practices, and culture towards a more equitable NYC.',
                ],
                modalList: [
                    {
                        linkTitle: 'Become a Court Watcher',
                        linkTarget: 'http://bit.ly/2wf0I2T',
                    },
                ],
            },

            {
                listTitle: 'Center for Court Innovation ',
                modalTitle: 'Center for Court Innovation ',
                modalCopy: [
                    `The Center for Court Innovation creates programs to test new ideas and solve problems for justice reformers around the world.`,
                ],
                modalList: [
                    {
                        linkTitle: 'Read More',
                        linkTarget: 'http://bit.ly/2QlaMAw',
                    },
                ],
            },
            {
                listTitle: 'Californians for Safety & Justice',
                modalTitle: 'Californians for Safety & Justice ',
                modalCopy: [
                    `Working to provide alternatives to incarceration that can increase neighborhood safety and build healthy communities while stopping the cycle of crime.`,
                ],
                modalList: [
                    {
                        linkTitle: 'Check Out Resources',
                        linkTarget: 'http://bit.ly/2YNy6dr',
                    },
                ],
            },
            {
                listTitle: 'Vera Institute of Justice ',
                modalTitle: 'Vera Institute of Justice ',
                modalCopy: [
                    'The Vera Institute of Justice works with others who share their vision to tackle the most pressing injustices of our day—from the causes and consequences of mass incarceration, racial disparities, and the loss of public trust in law enforcement, to the unmet needs of the vulnerable, the marginalized, and those harmed by crime and violence.Unlocking the Black Box of Prosecution',
                ],
                modalList: [
                    {
                        linkTitle: 'Read More',
                        linkTarget: 'http://bit.ly/2wcNW58',
                    },
                ],
                secondSet: {
                    title: 'Unlocking the Black Box of Prosecution',
                    copy:
                        'The Vera Institute works to improve justice systems that ensure fairness, promote safety, and strengthen communities. ',
                    link: {
                        copy: 'Read More',
                        target: 'http://bit.ly/2wcNW58',
                    },
                },
            },

            {
                listTitle: 'Pretrial Justice Institute',
                modalTitle: 'Pretrial Justice Institute',
                modalCopy: [
                    'Pretrial Justice Institute serves as a resource for common pretrial justice challenges and ensuring that local systems produce fair outcomes.',
                ],
                modalList: [
                    {
                        linkTitle: 'Learn More',
                        linkTarget: 'http://bit.ly/2HPSgfU',
                    },
                ],
            },
        ],
    },
    {
        rightSideImage: false,
        image: incarceration,
        episode: 'Part Three',
        title: 'INCARCERATION',
        copy:
            'The indignity and inhumanity of America’s mass incarceration system result in pain and generational trauma while failing to promote public safety. These organizations are fighting for the rights and dignity of incarcerated children and people, and an end to mass incarceration:',
        episodeLinks: [
            {
                listTitle: 'The Innocence Project',
                modalTitle: 'The Innocence Project',
                modalCopy: [
                    'The Innocence Project, founded in 1992 by Peter Neufeld and Barry Scheck at Cardozo School of Law, exonerates the wrongly convicted through DNA testing and reforms the criminal justice system to prevent future injustice.',
                ],
                modalList: [
                    {
                        linkTitle: 'Get Involved',
                        linkTarget: 'http://bit.ly/2JC0OKL',
                    },
                    {
                        linkTitle:
                            'Learn more about how the legal system and media robbed five innocent teens of their presumption of innocence and so much more.',
                        linkTarget: 'http://bit.ly/2QDMu5j',
                    },
                ],
                endCopy: {
                    copy:
                        'Learn more about how the legal system and media robbed five innocent teens of their presumption of innocence and so much more.',
                    linkTitle: null,
                    linkTarget: null,
                },
            },
            {
                listTitle: 'National Coalition to Abolish The Death Penalty',
                modalTitle: 'National Coalition to Abolish The Death Penalty',
                modalCopy: [
                    'The National Coalition to Abolish the Death Penalty’s mission is to abolish the death penalty in the United States and support efforts to abolish the death penalty worldwide.',
                ],
                modalList: [
                    {
                        linkTitle: 'Take Action',
                        linkTarget: 'http://bit.ly/2HAGG9L',
                    },
                ],
            },
            {
                listTitle: 'Essie Justice Group',
                modalTitle: 'Essie Justice Group',
                modalCopy: [
                    'Essie Justice Group is a non - profit organization of women with incarcerated loved ones taking on the rampant injustices created by mass incarceration. Their Healing to Advocacy Model brings women together to heal, build collective power, and drive social change.They are building a membership of fierce advocates for race and gender justice — including Black and Latinx women, formerly and currently incarcerated women, Transwomen, and gender non - conforming people.',
                ],
                modalList: [
                    {
                        linkTitle: 'Learn More',
                        linkTarget: 'http://bit.ly/2VWIBhz',
                    },
                ],
            },
            {
                listTitle: 'Women’s Prison Association',
                modalTitle: 'Women’s Prison Association',
                modalCopy: [
                    'WPA helps women in all stages of the criminal justice system from housing and employment to complying with criminal justice mandates.',
                ],
                modalList: [
                    {
                        linkTitle: 'Learn More',
                        linkTarget: 'http://bit.ly/2HyLQ5W',
                    },
                ],
            },
            {
                listTitle: 'FWD.us',
                modalTitle: 'FWD.us',
                modalCopy: [
                    'FWD.us approaches reform by specifically utilizing the power of technology and business to influence policymakers.',
                ],
                modalList: [
                    {
                        linkTitle: 'Learn More',
                        linkTarget: 'http://bit.ly/2VKzRpN',
                    },
                ],
            },
            {
                listTitle: 'Campaign for Youth Justice',
                modalTitle: 'Campaign for Youth Justice',
                modalCopy: [
                    'CFYJ works to end prosecuting, sentencing, and incarcerating under 18 youth in adult criminal justice systems.',
                ],
                modalList: [
                    {
                        linkTitle: 'Share Your Story',
                        linkTarget: 'http://bit.ly/2M7oxoj',
                    },
                ],
            },
            {
                listTitle: 'We Got Us Now',
                modalTitle: 'We Got Us Now',
                modalCopy: [
                    'We Got Us Now focuses on improving accessibility so families can remain connected while impacted by the criminal justice system.',
                ],
                modalList: [
                    {
                        linkTitle: 'Learn More',
                        linkTarget: 'http://bit.ly/2I8u1K3',
                    },
                    {
                        linkTitle:
                            'Sign The Petition and Help #KeepFamiliesConnected',
                        linkTarget: 'http://bit.ly/2WlR4u6',
                    },
                ],
            },
            {
                listTitle: 'The Sentencing Project',
                modalTitle: 'The Sentencing Project',
                modalCopy: [
                    'Working to promote reforms around sentencing policies, unjust racial disparities and practices and incarceration alternatives.',
                ],
                modalList: [
                    {
                        linkTitle: 'Get Involved with Local Reform',
                        linkTarget: 'http://bit.ly/2HxnFoC',
                    },
                ],
            },
        ],
    },
    {
        rightSideImage: true,
        image: postIncarceration,
        episode: 'Part Four',
        title: 'POST-INCARCERATION',
        copy:
            'People leave incarceration hobbled by systemic and cultural discrimination and face legal barriers to jobs, housing and education that hinder their success. These organizations are supporting formerly incarcerated people: ',
        episodeLinks: [
            {
                listTitle: 'Vera Institute of Justice',
                modalTitle: 'Vera Institute of Justice',
                modalCopy: [
                    'The Vera Institute of Justice works with others who share their vision to tackle the most pressing injustices of our day—from the causes and consequences of mass incarceration, racial disparities, and the loss of public trust in law enforcement, to the unmet needs of the vulnerable, the marginalized, and those harmed by crime and violence.',
                ],
                modalList: [
                    {
                        linkTitle: null,
                        linkTarget: null,
                    },
                ],
                secondSet: {
                    title: 'Reimagining Prison Interactive Web Report',
                    copy:
                        'Incarceration in America causes individual, community, and generational pain and deprivation. Built on a system of racist policies and practices that has disproportionately impacted people of color, mass incarceration has decimated communities and families. But the harsh conditions within prisons neither ensure safety behind the walls nor prevent crime and victimization. In this report, Vera reimagines the how, what, and why of incarceration and asserts a new governing principle on which to ground prison policy and practice: human dignity. In this groundbreaking report, Vera shows that we can reimagine prisons and incarceration in this country - and that we must.',
                    link: {
                        copy: 'View the report ',
                        target: 'http://bit.ly/2QjvCk1',
                    },
                },
                thirdSet: {
                    title: 'Incarceration Trends Project ',
                    copy:
                        'Although we are in a widely recognized era of criminal justice reform, decline in incarceration masks distinct trends that vary from state to state and county to county. Some states have meaningfully reduced incarceration, but others are stuck near all-time highs, continuing to incarcerate more and more people, or are shifting populations between prisons and jails. This is a great opportunity for people to understand what’s happening in their own community when it comes to mass incarceration and is a fantastic side-by-side for Color of Change’s Winning Justice. Once people understand how their community is fueling mass incarceration, they can look up their own elected officials and prosecutors and contact them! ',
                    link: {
                        copy: 'Learn More',
                        target: 'http://bit.ly/2M7XDwn',
                    },
                },
            },
            {
                listTitle: 'Alliance for Safety and Justice ',
                modalTitle: 'Alliance for Safety and Justice ',
                modalCopy: [
                    ' ASJ approaches reform on a state-level to help support communities and reduce incarceration and racial disparities in the justice system.',
                ],
                modalList: [
                    {
                        linkTitle: 'Learn More',
                        linkTarget: 'http://bit.ly/2whL0UW',
                    },
                ],
            },
            {
                listTitle: 'A New Way of Life Reentry Project',
                modalTitle: 'A New Way of Life Reentry Project',
                modalCopy: [
                    'Providing housing for women whom were formerly incarcerated, A New Way of Life Reentry Project uses housing, legal, policy, and leadership support to create social change.',
                ],
                modalList: [
                    {
                        linkTitle: 'Sign Up to Volunteer',
                        linkTarget: 'http://bit.ly/2Wo5Y34',
                    },
                ],
            },
            {
                listTitle: 'Arts for Incarcerated Youth Network (Los Angeles)',
                modalTitle: 'Arts for Incarcerated Youth Network (Los Angeles)',
                modalCopy: [
                    'Using interdisciplinary programming, AIYN focuses on transforming juvenile justice systems via building resiliency and wellness.',
                ],
                modalList: [
                    {
                        linkTitle: 'Read More',
                        linkTarget: 'http://bit.ly/2HzA63b',
                    },
                ],
            },
            {
                listTitle: 'Common Justice',
                modalTitle: 'Common Justice',
                modalCopy: [
                    'Common Justice focuses on reducing violence without incarceration while maintaining accountability and providing victim-service programs.',
                ],
                modalList: [
                    {
                        linkTitle: 'Read More',
                        linkTarget: 'http://bit.ly/2JZzUvS',
                    },
                ],
            },
            {
                listTitle: 'Operation Restoration',
                modalTitle: 'Operation Restoration',
                modalCopy: [
                    'Empowering women and girls, Operation Restoration supports individuals reentering into higher education to foster long term success.',
                ],
                modalList: [
                    {
                        linkTitle: 'Sign up to Volunteer',
                        linkTarget: 'http://bit.ly/2W0uDvc',
                    },
                ],
            },
            {
                listTitle: 'Youth Justice Fund',
                modalTitle: 'Youth Justice Fund',
                modalCopy: [
                    'YJF provides housing, health, transportation, education and employment resources for youth disproportionately impacted by the criminal justice system.',
                ],
                modalList: [
                    {
                        linkTitle: 'Sign Up to Volunteer',
                        linkTarget: 'http://bit.ly/2HxaOCB',
                    },
                ],
            },
            {
                listTitle: 'Campaign for the Fair Sentencing of Youth',
                modalTitle: 'Campaign for the Fair Sentencing of Youth',
                modalCopy: [
                    'CFSY has many approaches to reform including serving as a hub and convener, engaging key stakeholders, educating target audiences, advocating for reforms, and bolstering legal strategies to ban life - without - parole sentences for children.',
                ],
                modalList: [
                    {
                        linkTitle: 'Learn More',
                        linkTarget: 'http://bit.ly/2WYfczY',
                    },
                ],
            },
            {
                listTitle: 'Just Leadership',
                modalTitle: 'Just Leadership',
                modalCopy: [
                    'Just Leadership works to cut the US correctional population in #halfby2030. JLUSA empowers people most affected by incarceration to drive policy reform.',
                ],
                modalList: [
                    {
                        linkTitle: 'Learn More',
                        linkTarget: 'http://bit.ly/2HBvHgf',
                    },
                ],
            },
        ],
    },
]
export default EpisodeData
